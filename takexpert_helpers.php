<?php

defined('ABSPATH') || die('No Access');

// auto redirect login page to previous page
add_action('login_auto_redirect', 'wupp_login_auto_redirect_hook');
function wupp_login_auto_redirect_hook(){
    ?>
    <a href="
    <?php echo esc_url(site_url('/login').'/?redirect_to='.get_permalink(get_the_id())); ?>"
    class="btn btn-primary user">ورود | عضویت</a>
    <?php
}

add_shortcode('login_auto_redirect', 'wupp_login_auto_redirect_shortcode');
function wupp_login_auto_redirect_shortcode(){
    return esc_url(site_url('/login').'/?redirect_to='.get_permalink(get_the_id()));
}

// check user exist (user panel pro plugin)
add_action('wp_ajax_wupp_check_user_exists', 'wupp_check_user_exists');
add_action('wp_ajax_nopriv_wupp_check_user_exists', 'wupp_check_user_exists');
function wupp_check_user_exists() {
    $user_exist = false;
    $user_login = sanitize_text_field( $_POST['user_login'] );
    if( preg_match( '/09[0-9]{9}/', $user_login ) ) {
        $user_login = substr( $user_login, 1 );
    }
    if( preg_match( '/9[0-9]{9}/', $user_login ) && get_user_by( 'login', $user_login ) ) {
            return wp_send_json_success();
    } else if( filter_var( $user_login, FILTER_VALIDATE_EMAIL) && get_user_by( 'email', $user_login ) ) {
        return wp_send_json_success();
    }
    return wp_send_json_error();
}

// show login/panel button text dynamicly
add_shortcode('login_or_panel_button', 'wupp_login_or_panel_button_shortcode');
function wupp_login_or_panel_button_shortcode(){
    ob_start();
    echo is_user_logged_in() ? 'پنل کاربری':'ورود';
    return ob_get_clean();
}

