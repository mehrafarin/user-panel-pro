<div class="container mt-4 mb-5">
    <div class="card border-0">
        <div class="card-header">
            <p class="m-0"><?php echo __("Bought details", "user-panel-pro");?></p>
            <button class="btn btn-sm btn-success position-absolute tt-version"
                    onclick="load_edd_payment_list('<?php echo $payment_key;?>')"
                    style="top: 8px !important;"><?php echo __("Back", "user-panel-pro");?></button>
        </div>
        <div class="card-body pr-5 pl-5 pt-3 pb-3">
			<?php include WUPP_TPL . 'panel/edd/billings/details-content.php' ?>
        </div>
    </div>
</div>